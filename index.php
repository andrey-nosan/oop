<?php

ini_set('display_errors', 1);

//phpinfo();

require "vendor/autoload.php";

//require_once "solid/l.php";

$connection = "mysql:host=localhost;port=3306;dbname=Chinook";

$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
];

$pdo = new PDO($connection, 'mydbuser', 'mydbuser_pass', $options);

$mediaTypeId = rand(1, 5);
$milliseconds = rand(100000, 900000);
$unitPrice = array_rand([0.99, 1.99, 0.59, 1.39]);

$deleteStmt = $pdo->prepare("DELETE FROM `Track` WHERE `TrackId` = :trackId LIMIT 1");

$deleteStmt->bindValue(':trackId', 3509);




$pdo->beginTransaction();

//DELETE DeliveryService 'ST'
//DISABLE Category 'ST'

$deleteStmt->execute();

//try {
//
//
//
//    //some logic
//    $pdo->commit();
//} catch (Exception $exception) {
//    $pdo->rollBack();
//}




if (isset($_GET['allow']) && $_GET['allow'] == 'yes') {
    $pdo->commit();
} else {
    $pdo->rollBack();
    echo "Rolled back!<br>";
}


//$updateStmt = $pdo->prepare("UPDATE `Track` SET `UnitPrice` = :unitPrice WHERE `TrackId` = :trackId LIMIT 1");
//
////$updateStmt->bindValue(':name', 'New Track 6');
//$updateStmt->bindValue(':unitPrice', 1.39);
//$updateStmt->bindValue(':trackId', 3504);
//
//$updateStmt->execute();

//$insertStmt = $pdo->prepare(
//    "INSERT INTO `Track` (`AlbumId`, `Name`, `MediaTypeId`, `GenreId`, `Milliseconds`, `UnitPrice`)
// VALUES (:albumId, :name, :mediaTypeId, :genreId, :milliseconds, :unitPrice)"
//);
//
//$insertStmt->bindValue(':name', 'New Track 5');
//$insertStmt->bindValue(':albumId', 85);
//$insertStmt->bindValue(':mediaTypeId', $mediaTypeId);
//$insertStmt->bindValue(':genreId', 10);
//$insertStmt->bindValue(':milliseconds', $milliseconds);
//$insertStmt->bindValue(':unitPrice', $unitPrice);
//
//$insertStmt->execute();

$genreId = 10;
$albumId = 85;

$columns = [
    'TrackId',
    'Name',
    'Composer',
    'UnitPrice',
];

$select = implode(',', $columns);

$statement = $pdo->prepare("SELECT {$select} FROM `Track` WHERE GenreId = ? AND AlbumId = ?");

$statement->bindParam(2, $albumId);
$statement->bindParam(1, $genreId);

$statement->execute();

$tracks = $statement->fetchAll();

foreach ($tracks as $index => $track) {
    $number = $index + 1;
    $composer = $track['Composer'] ?? 'No composer';
    echo "{$number} | {$track['TrackId']} | {$track['Name']} | {$composer} | {$track['UnitPrice']}<br>";
}




//use App\Classes\Car;
//use App\Classes\Bicycle;
//use App\Classes\Track;
//
//$car = (new Car());
//
//$bicycle = new Bicycle();

//$track = new Track();
//$track->run();

//$car->connect();
//$car->refill(767);
//$car->disconnect();
//$car->run();
//
//$bicycle->refill(67);
//$bicycle->run();

