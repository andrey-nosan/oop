<?php

use App\Classes\Solid\S\Post;

$post = new Post('Title 1', 'Content 1');

header('Content-Type: application/json');
echo $post->toJson();

//header('Content-Type: application/xml');
//echo $post->toXml();