<?php


use App\Classes\Solid\O\APIOrderSource;
use App\Classes\Solid\O\CURLOrderSource;
use App\Classes\Solid\O\MySQLOrderSource;
use App\Classes\Solid\O\OrderRepository;

if (empty($_GET['source'])) {
    exit('Please choose data source');
}

switch ($_GET['source']) {
    case 'mysql':
    default :
        $source = new MySQLOrderSource();
        break;
    case 'api':
        $source = new APIOrderSource();
        break;
    case 'curl':
        $source = new CURLOrderSource();
        break;
}


//Client
$orderRepository = new OrderRepository($source);

var_dump($orderRepository->load(77777));