<?php

namespace App\Classes\Solid\O;

use App\Contracts\IOrderSource;

class MySQLOrderSource implements IOrderSource
{
    public function load(int $orderID): array
    {
        //obtain data from MySQL
        return [__CLASS__, $orderID];

//        $pdo = new \PDO("mysql:host=localhost;port=3306;dbname=Shop");
//        $statement = $pdo->prepare('SELECT * FROM `orders` WHERE id=:id');
//        $statement->execute(array(':id' => $orderID));
//
//        return $statement->fetchAll();
    }

    public function save($order)
    {
        // TODO: Implement save() method.
    }

    public function update($order)
    {
        // TODO: Implement update() method.
    }

    public function delete(int $orderID): bool
    {
        // TODO: Implement delete() method.
        return false;
    }
}