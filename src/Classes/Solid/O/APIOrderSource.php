<?php

namespace App\Classes\Solid\O;

class APIOrderSource implements \App\Contracts\IOrderSource
{

    public function load(int $orderID): array
    {
        //obtain data from API
        return [__CLASS__, $orderID];
    }

    public function save($order)
    {
        // TODO: Implement save() method.
    }

    public function update($order)
    {
        // TODO: Implement update() method.
    }

    public function delete(int $orderID): bool
    {
        // TODO: Implement delete() method.
        return false;
    }
}