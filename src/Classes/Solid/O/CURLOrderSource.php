<?php

namespace App\Classes\Solid\O;

use App\Contracts\IOrderSource;

class CURLOrderSource implements IOrderSource
{

    public function load(int $orderID): array
    {
        return [__CLASS__, $orderID];
        // TODO: Implement load() method.
    }

    public function save($order)
    {
        // TODO: Implement save() method.
    }

    public function update($order)
    {
        // TODO: Implement update() method.
    }

    public function delete(int $orderID): bool
    {
        // TODO: Implement delete() method.
        return false;
    }
}