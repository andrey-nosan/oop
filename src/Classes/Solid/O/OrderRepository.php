<?php

namespace App\Classes\Solid\O;

use App\Contracts\IOrderSource;

class OrderRepository
{
    private IOrderSource $source;

    /**
     * @param IOrderSource $source
     */
    public function __construct(IOrderSource $source)
    {
        $this->source = $source;
    }

    public function load($orderID): array
    {
        return $this->source->load($orderID);
    }



    public function save($order){/*...*/}
    public function update($order){/*...*/}
    public function delete($order){/*...*/}
}