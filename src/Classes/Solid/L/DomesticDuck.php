<?php

namespace App\Classes\Solid\L;

class DomesticDuck extends Duck
{
    protected $speed;

    /**
     * @param $speed
     */
    public function __construct($speed)
    {
        parent::__construct($speed);
        $this->speed = $speed;
    }

    private function runSpeed()
    {
        return __CLASS__ . ' => Run speed: ' . $this->speed;
    }

    public function moveSpeed()
    {
        return $this->runSpeed();
    }
}