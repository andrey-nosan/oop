<?php

namespace App\Classes\Solid\L;

class Duck extends Bird
{
    protected $speed;

    /**
     * @param $speed
     */
    public function __construct($speed)
    {
        $this->speed = $speed;
    }


    public function moveSpeed()
    {
        // TODO: Implement flySpeed() method.
        return __CLASS__ . ' => Fly speed: ' . $this->speed;
    }
}