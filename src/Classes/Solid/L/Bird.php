<?php

namespace App\Classes\Solid\L;

abstract class Bird
{
    protected $speed;

    abstract public function moveSpeed();
}