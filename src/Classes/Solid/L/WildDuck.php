<?php

namespace App\Classes\Solid\L;

class WildDuck extends Duck
{
    protected $speed;

    /**
     * @param $speed
     */
    public function __construct($speed)
    {
        parent::__construct($speed);
        $this->speed = $speed;
    }

    private function flySpeed()
    {
        return __CLASS__ . ' => Fly speed: ' . $this->speed;
    }

    public function moveSpeed()
    {
        return $this->flySpeed();
    }
}