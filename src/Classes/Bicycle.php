<?php

namespace App\Classes;


class Bicycle extends Vehicle
{
    public function run()
    {
        if (!$this->refilled) {
            echo 'Bicycle is not charged!';
        } else {
            echo 'Bicycle goes!';
        }

        echo '<br>';
    }

    public function refill(float $fuelVolume = 0)
    {
        if ($fuelVolume > 0) {
            $this->refilled = true;
        }
    }
}