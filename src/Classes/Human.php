<?php

namespace App\Classes;
class Human
{
    const CITY = 'Kyiv';

    private string $first_name;
    private string $last_name;
    private string $age;

    private static string $address = '';

    public function __construct(string $first_name, string $last_name, ?int $age = null)
    {
        var_dump(__METHOD__);
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->age = $age ?? 'Not specified';
    }

    public function getFullName(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getAge()
    {
        $this->example();

        return $this->age;
    }

    private function example(): void
    {
        var_dump(__METHOD__);
        var_dump(self::CITY);
    }

    public static function setAddress(string $address)
    {
        self::$address = $address;
    }

    /**
     * @return string
     */
    public static function getAddress(): string
    {
        return self::$address;
    }
}