<?php

namespace App\Classes;

abstract class Vehicle
{
    protected bool $refilled = false;
    protected bool $connected = false;

    abstract public function refill(float $fuelVolume = 0);

    public function connect()
    {
        $this->connected = true;
    }

    public function disconnect()
    {
        $this->connected = false;
    }
}