<?php

namespace App\Classes;


class Car extends Vehicle
{

    public function run()
    {
        if (!$this->refilled) {
            echo 'Car is not refilled!';
        } elseif ($this->connected) {
            echo 'Car is still connected to pump!';
        } else {
            echo 'Car goes!';
        }

        echo '<br>';
    }

    public function refill(float $fuelVolume = 0)
    {
        

        if ($this->connected && $fuelVolume > 0) {
            $this->refilled = true;
        }
    }


}