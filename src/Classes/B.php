<?php

namespace Classes;

use App\Traits\Trait2;

class B extends A
{
    use Trait2;

    public static function who() {
        echo __CLASS__;
    }
}