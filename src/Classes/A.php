<?php

namespace App\Classes;

use App\Traits\Trait1;
use App\Traits\Trait2;

class A
{
    use Trait1, Trait2 {
        Trait1::sayHello insteadof Trait2;
        Trait2::sayHello as newSayHello;
    }

    private $label;

    public static function who() {
        echo __CLASS__;
    }

    public function someThing()
    {
        echo 'ST';
    }

    public function test()
    {
//        static::who();
        $this->someThing();
    }

    /**
     * @param mixed $label
     * @return A
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return void
     */
    public function printLabel()
    {
        echo $this->label;
    }
}