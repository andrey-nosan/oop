<?php

namespace App\Traits;

trait Serializable
{
//    public function toArray()
//    {
//        return get_object_vars($this);
//    }

    public $db;

    public function toJson()
    {
        return json_encode(get_object_vars($this), JSON_PRETTY_PRINT);
    }

    public function toXml()
    {
        $xml = '<?xml version="1.1" encoding="UTF-8"?><root><post>';

        $properties = get_object_vars($this);

        foreach ($properties as $property => $value) {
            $xml .= "<{$property}>{$value}</{$property}>";

            //<title>Title 1</title>
        }

        $xml .= '</post></root>';

        return $xml;
    }
}