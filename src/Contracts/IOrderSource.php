<?php

namespace App\Contracts;

interface IOrderSource
{
    public function load(int $orderID): array;

    public function save($order);
    public function update($order);
    public function delete(int $orderID): bool;
}
